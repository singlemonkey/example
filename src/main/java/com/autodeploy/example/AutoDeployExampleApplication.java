package com.autodeploy.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoDeployExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoDeployExampleApplication.class, args);
    }

}
