package com.autodeploy.example.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "system-setting")
public class SystemSetting {
    private String dbExampleHost;
    private String dbExamplePort;
    private String appVersion;
}
