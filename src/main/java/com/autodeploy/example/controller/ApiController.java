package com.autodeploy.example.controller;

import com.autodeploy.example.config.SystemSetting;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
@Slf4j
public class ApiController {

    private final SystemSetting systemSetting;

    @GetMapping("/getSystemSetting")
    public SystemSetting getAppName() {

        log.debug("系统版本号：" + systemSetting.getAppVersion());
        log.debug("数据库地址：" + systemSetting.getDbExampleHost());
        log.debug("数据库端口号：" + systemSetting.getDbExamplePort());

        return systemSetting;
    }
}
