FROM openjdk:17
LABEL authors="zhangqi"

WORKDIR /app

COPY target/auto-deploy-example.jar /app/auto-deploy-example.jar

EXPOSE 9031

CMD ["java","-jar","/app/auto-deploy-example.jar","--spring.profiles.active=${PROFILE}"]