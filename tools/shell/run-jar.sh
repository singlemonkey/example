#!/bin/bash
echo "---请确认已正确配置env文件环境变量内容---"
PID=$(ps aux | grep project-name.jar | grep -v grep | awk '{print $2}')
if [ -n "${PID}" ]; then
  echo "停止旧应用"
  kill -9 "$PID"
fi
echo "导出应用所需环境变量"
export $(cat env | xargs)

if [ -d "tools/jdk/jdk-17.0.8/" ]; then
  echo "jdk已安装"
else
  arch=$(uname -m)
  # 判断是否为ARM架构
  if [ "$arch" == "aarch64" ] || [ "$arch" == "armv7l" ]; then
    echo "ARM架构服务器"
    tar zxvf tools/jdk/jdk-17_linux-aarch64_bin.tar.gz -C tools/jdk/
  else
    echo "非ARM架构服务器"
    tar zxvf tools/jdk/jdk-17_linux-x64_bin.tar.gz -C tools/jdk/
  fi
fi

echo "启动应用"
rm -rf nohup.out
nohup tools/jdk/jdk-17.0.8/bin/java -Xms512m -Xmx1024m -jar -Duser.timezone=GMT+08 project-name.jar --spring.profiles.active=prd >/dev/null 2>&1 &
echo "应用启动完成，查看日志请前往/opt/logs/project-name_log"
